package br.com.selecaojava.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author claudio
 */
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long codigo;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Column(name = "data_coleta")
    private Date datadaColeta;

    @NumberFormat(pattern = "#,##0.0000")
    private BigDecimal valorCompra;

    @NumberFormat(pattern = "#,##0.0000")
    private BigDecimal valorVenda;

    private String tipoProduto;

    @ManyToOne
    @JoinColumn(name = "id_distribuidora")
    private Distribuidora distribuidora;

}
