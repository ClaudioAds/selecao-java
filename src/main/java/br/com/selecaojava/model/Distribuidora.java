package br.com.selecaojava.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author claudio
 */
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class Distribuidora {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToMany(mappedBy = "distribuidora")
    private List<Endereco> enderecos;

    private String instalacaoRazaoSocial;

    @OneToMany(mappedBy = "distribuidora")
    private List<Produto> produtos = new ArrayList<>();

    private String unidadedeMedida;

    private String bandeiraDistribuidora;

}
