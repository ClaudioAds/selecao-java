package br.com.selecaojava.controller;

import br.com.selecaojava.model.Distribuidora;
import br.com.selecaojava.repository.DistribuidoraRepository;
import br.com.selecaojava.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author claudio
 */
@RestController
@RequestMapping("distribuidoras")
public class DistribuidoraEndpoint {
    private final DistribuidoraRepository distribuidoraRepository;
    private UploadService uploadService;

    @Autowired
    public DistribuidoraEndpoint(DistribuidoraRepository distribuidoraRepository) {
        this.distribuidoraRepository = distribuidoraRepository;
    }

    @GetMapping
    public ResponseEntity<?> listAll(Pageable pageable) {
        return new ResponseEntity<>(distribuidoraRepository.findAll(pageable), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> listForId(@PathVariable("id") Long id) {
        return new ResponseEntity<>(distribuidoraRepository.findById(id), HttpStatus.OK);
    }

    @GetMapping(path = "/findByNome/{nome}")
    public ResponseEntity<?> findByNome(@PathVariable String sigla) {
        return new ResponseEntity<>(distribuidoraRepository.findByBandeiraDistribuidoraStartsWith(sigla), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody Distribuidora distribuidora) {
        return new ResponseEntity<>(distribuidoraRepository.save(distribuidora), HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        distribuidoraRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody Distribuidora distribuidora) {
        distribuidoraRepository.save(distribuidora);
        return new ResponseEntity<>(distribuidora, HttpStatus.OK);
    }

    @GetMapping(path = "/municipio")
    public ResponseEntity<?> findMediaForCity(@RequestBody String cidade) {
        distribuidoraRepository.findMediaForCity(cidade);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path = "/siglaregiao")
    public ResponseEntity<?> findAllForRegion(@PathVariable String siglaRegiao) {
        distribuidoraRepository.findAllForRegion(siglaRegiao);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path = "/groupdistribuidora")
    public ResponseEntity<?> findAllGroupForDistribuidora() {
        distribuidoraRepository.findAllGroupForDistribuidora();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path = "/datacoleta")
    public ResponseEntity<?> findAllOnGroupByDataColeta(String data) throws ParseException {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String dataRecebida = data;
        Date dataOk = formato.parse(dataRecebida);
        distribuidoraRepository.findAllOnGroupByDataColeta(dataOk);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path = "/mediaprecosvendaecompracidade")
    public ResponseEntity<?> findMediaPurchesePriceAndSalePriceForCity(String cidade) {
        distribuidoraRepository.findMediaPurchesePriceAndSalePriceForCity(cidade);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path = "/bandeira")
    public ResponseEntity<?> findMediaPurchesePriceAndSalePriceForStandard(String bandeira) {
        distribuidoraRepository.findMediaPurchesePriceAndSalePriceForStandard(bandeira);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}