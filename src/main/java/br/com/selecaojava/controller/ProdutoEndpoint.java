package br.com.selecaojava.controller;

import br.com.selecaojava.error.ResourceNotFoundException;
import br.com.selecaojava.model.Produto;
import br.com.selecaojava.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author claudio
 */
@RestController
@RequestMapping("produtos")
public class ProdutoEndpoint {

        private final ProdutoRepository produtoRepository;

        @Autowired
        public ProdutoEndpoint(ProdutoRepository produtoRepository) {
            this.produtoRepository = produtoRepository;
        }

    @GetMapping
        public ResponseEntity<?> listAll(Pageable pageable) {
            return new ResponseEntity<>(produtoRepository.findAll(pageable), HttpStatus.OK);
        }

        @GetMapping(path = "/{id}")
        public ResponseEntity<?> listForId(@PathVariable("id") Long id) {
            return new ResponseEntity<>(produtoRepository.findById(id), HttpStatus.OK);
        }


        @PostMapping(path = "/salvar")
        public ResponseEntity<?> save(@RequestBody Produto produto) {
            return new ResponseEntity<>(produtoRepository.save(produto), HttpStatus.CREATED);
        }

        @DeleteMapping(path = "/{id}")
        public ResponseEntity<?> delete(@PathVariable Long id) {
            produtoRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }

        @PutMapping
        public ResponseEntity<?> update(@RequestBody Produto produto) {
            produtoRepository.save(produto);
            return new ResponseEntity<>(produto, HttpStatus.OK);
        }


        public void verificaUsuarioException(Long id) {
            if (produtoRepository.findById(id) == null)
                throw new ResourceNotFoundException("Usuário não encontrado pelo id:" + id);
        }
}
