package br.com.selecaojava.controller;

import br.com.selecaojava.error.ResourceNotFoundException;
import br.com.selecaojava.model.Usuario;
import br.com.selecaojava.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author claudio
 */
@RestController
@RequestMapping("usuarios")
public class UsuarioEndPoint {
    private final UsuarioRepository usuarioRepository;

    @Autowired
    public UsuarioEndPoint(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    @GetMapping
    public ResponseEntity<?> listAll(Pageable pageable) {
        return new ResponseEntity<>(usuarioRepository.findAll(pageable), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> listForId(@PathVariable("id") Long id) {
        return new ResponseEntity<>(usuarioRepository.findById(id), HttpStatus.OK);
    }

    @GetMapping(path = "/findByNome/{nome}")
    public ResponseEntity<?> findByNome(@PathVariable String nome){
        return new ResponseEntity<>(usuarioRepository.findByNome(nome), HttpStatus.OK);
    }

    @PostMapping(path = "/salvar")
    public ResponseEntity<?> save(@RequestBody Usuario usuario) {
        return new ResponseEntity<>(usuarioRepository.save(usuario), HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        usuarioRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody Usuario usuario) {
        usuarioRepository.save(usuario);
        return new ResponseEntity<>(usuario, HttpStatus.OK);
    }


    public void verificaUsuarioException(Long id) {
        if (usuarioRepository.findById(id) == null)
            throw new ResourceNotFoundException("Usuário não encontrado pelo id:" + id);
    }

}
