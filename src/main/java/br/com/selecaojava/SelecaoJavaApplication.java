package br.com.selecaojava;

import br.com.selecaojava.model.Distribuidora;
import br.com.selecaojava.model.Endereco;
import br.com.selecaojava.model.Produto;
import br.com.selecaojava.repository.DistribuidoraRepository;
import br.com.selecaojava.repository.EnderecoRepository;
import br.com.selecaojava.repository.ProdutoRepository;
import org.hibernate.internal.CoreLogging;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootApplication
public class SelecaoJavaApplication {


    public static void main(String[] args) {
        SpringApplication.run(SelecaoJavaApplication.class, args);
    }
}


