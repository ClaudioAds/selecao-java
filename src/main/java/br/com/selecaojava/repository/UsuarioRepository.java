package br.com.selecaojava.repository;

import br.com.selecaojava.model.Usuario;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * @author claudio
 */
public interface UsuarioRepository extends PagingAndSortingRepository<Usuario, Long> {

    List<Usuario> findByNome(String nome);

}
