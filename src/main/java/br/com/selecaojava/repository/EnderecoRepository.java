package br.com.selecaojava.repository;

import br.com.selecaojava.model.Endereco;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author claudio
 */
@Repository
public interface EnderecoRepository extends JpaRepository<Endereco, String> {


}
