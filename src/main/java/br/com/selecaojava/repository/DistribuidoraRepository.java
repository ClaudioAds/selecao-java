package br.com.selecaojava.repository;

import br.com.selecaojava.model.Distribuidora;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author claudio
 */
public interface DistribuidoraRepository extends PagingAndSortingRepository<Distribuidora, Long> {
    //retorna a média de valores de venda de combustível
    // informando a cidade
    @Query(value = "SELECT avg(p.valor_venda), e.municipio_venda FROM produto  p JOIN Endereco E ON p.id_Distribuidora = e.id_distribuidora and e.municipio_venda LIKE %?1%", nativeQuery = true)
    List<Distribuidora> findMediaForCity(String cidade);

    //retorna todos os dados especificando a sigla da região
    @Query(value = "SELECT * FROM ENDERECO E JOIN DISTRIBUIDORA D ON E.ID_DISTRIBUIDORA  = D.ID JOIN PRODUTO P ON D.ID = P.ID_DISTRIBUIDORA AND E.SIGLA_REGIAO LIKE %?1%", nativeQuery = true)
    List<Distribuidora> findAllForRegion(String siglaRegiao);

    //retorna todos os dados da tabela agrupados por distribuidora
    @Query(value = "SELECT ID, BANDEIRA_DISTRIBUIDORA, INSTALACAO_RAZAO_SOCIAL, UNIDADEDE_MEDIDA FROM DISTRIBUIDORA GROUP BY INSTALACAO_RAZAO_SOCIAL,ID, BANDEIRA_DISTRIBUIDORA, UNIDADEDE_MEDIDA", nativeQuery = true)
    List<Distribuidora> findAllGroupForDistribuidora();

    //Tras todas as informações relacionadas a uma data de coleta
    @Query(value = "SELECT * FROM PRODUTO WHERE DATA_COLETA = ?1 GROUP BY DATA_COLETA, CODIGO", nativeQuery = true)
    Optional<Distribuidora> findAllOnGroupByDataColeta(Date data);

    //Tras a média de preços vendidos e comprados por cidade
    @Query(value = "select avg(p.valor_compra), avg(p.valor_venda)  FROM produto JOIN Endereco E ON p.id_Distribuidora = e.id_distribuidora and e.municipio_venda iLIKE %?1%", nativeQuery = true)
    List<Distribuidora> findMediaPurchesePriceAndSalePriceForCity(String municipio);

    //Tras a média de preços vendidos e comprados por bandeira
    @Query(value = "select avg(p.valor_compra), avg(p.valor_venda)  FROM produto  p JOIN distribuidora d ON p.id_Distribuidora = d.id and d.bandeira_distribuidora iLIKE %?1%", nativeQuery = true)
    List<Distribuidora> findMediaPurchesePriceAndSalePriceForStandard(String bandeira);


    List<Distribuidora> findByBandeiraDistribuidoraStartsWith(String sigla);

}
