package br.com.selecaojava.repository;

import br.com.selecaojava.model.Produto;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author claudio
 */
public interface ProdutoRepository extends PagingAndSortingRepository<Produto, Long> {

}
