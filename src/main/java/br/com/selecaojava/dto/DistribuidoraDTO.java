package br.com.selecaojava.dto;

import br.com.selecaojava.model.Endereco;
import br.com.selecaojava.model.Produto;
import lombok.Data;

import javax.persistence.Id;
import java.util.List;

/**
 * @author claudio
 */
@Data
public class DistribuidoraDTO {

    @Id
    private Long id;
    private List<Endereco> enderecos;
    private String instalacaoRazaoSocial;
    private List<Produto> produtos;
    private String unidadedeMedida;
    private String bandeiraDistribuidora;

}
