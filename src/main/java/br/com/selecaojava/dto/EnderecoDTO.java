package br.com.selecaojava.dto;

import br.com.selecaojava.model.Distribuidora;
import lombok.Data;

import javax.persistence.Id;

/**
 * @author claudio
 */
@Data
public class EnderecoDTO {

    @Id
    private Long id;
    private String siglaRegiao;
    private String siglaEstado;
    private String MunicipioRevenda;
    private Distribuidora distribuidora;
}
