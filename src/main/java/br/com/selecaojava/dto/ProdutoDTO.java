package br.com.selecaojava.dto;

import br.com.selecaojava.model.Distribuidora;
import lombok.Data;

import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author claudio
 */

@Data
public class ProdutoDTO {
    @Id
    private Long codigo;
    private Date dataColeta;
    private BigDecimal valorCompra;
    private BigDecimal valorVenda;
    private String tipoProduto;
    private Distribuidora distribuidora;
}
