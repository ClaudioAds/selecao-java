package br.com.selecaojava.service;

import br.com.selecaojava.model.Distribuidora;
import br.com.selecaojava.model.Endereco;
import br.com.selecaojava.model.Produto;
import br.com.selecaojava.repository.DistribuidoraRepository;
import br.com.selecaojava.repository.EnderecoRepository;
import br.com.selecaojava.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author claudio
 */
@Component
public class UploadService {

    private final EnderecoRepository enderecoRepository;
    private final ProdutoRepository produtoRepository;
    private final DistribuidoraRepository distribuidoraRepository;

    @Autowired
    public UploadService(EnderecoRepository enderecoRepository, ProdutoRepository produtoRepository, DistribuidoraRepository distribuidoraRepository) {
        this.enderecoRepository = enderecoRepository;
        this.produtoRepository = produtoRepository;
        this.distribuidoraRepository = distribuidoraRepository;
    }

    @Bean
    public String runner() {
        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("2018-1_CA.csv"))) {
            String line;
            int i = 0;

            while ((line = br.readLine()) != null) {

                String[] values = line.split(";");

                if (values.length == 10) {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    String dataRecebida = values[6];
                    Date data = formato.parse(dataRecebida);

                    Endereco endereco = new Endereco();
                    Distribuidora distribuidora = new Distribuidora();
                    Produto produto = new Produto();

                    produto.setCodigo(Long.parseLong(values[4]));
                    produto.setTipoProduto(values[5]);
                    produto.setDatadaColeta(data);
                    produto.setValorVenda(new BigDecimal(values[7]));


                    distribuidora.setInstalacaoRazaoSocial(values[3]);
                    distribuidora.setUnidadedeMedida(values[8]);
                    distribuidora.setBandeiraDistribuidora(values[9]);
                    distribuidora = this.distribuidoraRepository.save(distribuidora);
                    produto.setDistribuidora(distribuidora);

                    endereco.setMunicipioRevenda(values[2]);
                    endereco.setSiglaEstado(values[1]);
                    endereco.setDistribuidora(distribuidora);
                    endereco.setSiglaRegiao(values[0]);

                    this.enderecoRepository.save(endereco);
                    this.produtoRepository.save(produto);

                } else {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    String dataRecebida = values[6];
                    Date data = formato.parse(dataRecebida);

                    Endereco endereco = new Endereco();
                    Distribuidora distribuidora = new Distribuidora();
                    Produto produto = new Produto();

                    produto.setCodigo(Long.parseLong(values[4]));
                    produto.setTipoProduto(values[5]);
                    produto.setDatadaColeta(data);
                    produto.setValorCompra(new BigDecimal(values[7]));
                    produto.setValorVenda(new BigDecimal(values[8]));

                    this.produtoRepository.save(produto);

                    distribuidora.setInstalacaoRazaoSocial(values[3]);
                    distribuidora.setUnidadedeMedida(values[9]);
                    distribuidora.setBandeiraDistribuidora(values[10]);
                    distribuidora = this.distribuidoraRepository.save(distribuidora);


                    endereco.setMunicipioRevenda(values[2]);
                    endereco.setSiglaEstado(values[1]);
                    endereco.setDistribuidora(distribuidora);
                    endereco.setSiglaRegiao(values[0]);
                    this.enderecoRepository.save(endereco);
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return String.format("Upload Ok");
    }
}
